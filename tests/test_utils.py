import pytest as pytest

from src import utils


def test_is_prime():
    assert utils.is_prime(4) == False
    assert utils.is_prime(2) == True
    assert utils.is_prime(3) == True
    assert utils.is_prime(8) == False
    assert utils.is_prime(10) == False
    assert utils.is_prime(7) == True
    assert utils.is_prime(-3), "Negative numbers are not allowed"

    if __name__ == "__main__":
        pytest.main()


def test_cubic():
    assert utils.cubic(2) == 8
    assert utils.cubic(-2) == -8
    assert utils.cubic(2) != 4
    assert utils.cubic(-3) != 27


def test_say_hello():
    assert utils.say_hello("test") == "Hello, test"
    assert utils.say_hello("a") == "Hello, a"
    assert utils.say_hello("b") != "Hi, b"
    assert utils.say_hello("c") != "Hi, c"
